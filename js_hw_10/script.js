const gallery = document.querySelector('.password-form');

gallery.addEventListener('click', event => {
    if(event.target.previousElementSibling.type === 'password'){
        event.target.classList.replace('fa-eye', 'fa-eye-slash');
        event.target.previousElementSibling.setAttribute('type', 'text');
    } else if (event.target.previousElementSibling.type === 'text'){
        event.target.classList.replace('fa-eye-slash', 'fa-eye');
        event.target.previousElementSibling.setAttribute('type', 'password');
    }
});

let submit = document.querySelector('.btn');

submit.addEventListener('click', () => {
    const inputsValue = document.querySelectorAll('input');
    if (inputsValue[0].value === '') {
        let span = document.createElement('span');
        span.classList.add('errPw');
        span.innerHTML = 'Нужно ввести хотя бы 1 символ';
        document.querySelector('.password-form').after(span);
        setTimeout( ()=>{
            span.remove()
        }, 2000);
    } else if (inputsValue[0].value == inputsValue[1].value) {
        alert("You are welcome");
    } else{
        let span = document.createElement('span');
        span.classList.add('errPw');
        span.innerHTML = 'Нужно ввести одинаковые значения';
        document.querySelector('.password-form').before(span);
        setTimeout(()=>{
            span.remove()
        }, 2000)
    }
});