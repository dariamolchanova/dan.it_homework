const tabs = document.querySelector('.tabs');
const tabsText = document.querySelectorAll('.tabs-content li')
tabs.addEventListener('click', (event) => {
    event.target.closest('ul').querySelector('.active').classList.remove('active');
    event.target.classList.add('active');
    tabsText.forEach(item => {
        console.log(item.dataset.text);
        item.setAttribute('hidden', true)
        if (item.dataset.text === event.target.dataset.name) {
            item.removeAttribute('hidden');
        }
    })
});

