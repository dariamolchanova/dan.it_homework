function arrayToLi(array, parent = document.body) {
    const mapped = array.map(element => `<li>${element}</li>`);
    parent.insertAdjacentHTML('afterbegin', `<ul>${mapped.join('')}</ul>`)
}
const newArray = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const parentElement = document.getElementById('divToInsert');

arrayToLi(newArray, parentElement)