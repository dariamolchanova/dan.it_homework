const form = document.createElement('form');
const price = document.createElement('input');
form.innerText = 'Price, $: '
form.appendChild(price);
document.body.appendChild(form);
form.classList.add('form');

price.classList.add('text');
price.placeholder = 'Price'
price.type = 'number'

price.addEventListener("focus", () => {
    const invalidText = document.querySelector('p')
    price.classList.add('focused')
    if (invalidText) {
        invalidText.remove();
        price.classList.remove('invalid');
    }
});
    
price.addEventListener("blur", () => {
    console.log(price.value);
    if (price.value === '') {
        price.classList.remove('focused')
    } else if (price.value < 0) {
        price.classList.remove('focused')
        price.classList.add('invalid'),
        form.insertAdjacentHTML("afterend", `<p>Please enter correct price</p>`);
    } else {
        const span = document.createElement('span');
        const button = document.createElement('button');

        span.innerText = `Текущая цена: ${price.value}`;
        button.innerText = 'X';
        span.appendChild(button);

        form.before(span);
        button.addEventListener('click', ()=> removeElement(span))
    }
    
});

function removeElement (element) {
    element.remove();
    price.value = '';
}
