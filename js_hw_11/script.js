const btns = document.querySelectorAll('.btn');

document.body.addEventListener('keypress', pressBtn )

function pressBtn ( event ) {
    btns.forEach((btn)=>{
        if (btn.classList.contains('btn-active')) {
            btn.classList.remove('btn-active');
        }
        if (btn.innerText.toLocaleLowerCase() === event.key.toLocaleLowerCase()) {
            btn.classList.add('btn-active');
        }
    });
}
  