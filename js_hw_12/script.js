const images = document.querySelector('.images-wrapper');
const btnStart = document.querySelector('#btnStart');
const btnStop = document.querySelector('#btnStop');
const timer = document.querySelector('#timer');
const timerValue = 3000;
let currentImgIndex = 0;
let timeToNext = timerValue;
let interval = createInterval();


btnStart.addEventListener('click', () => {
    interval = createInterval();
});

btnStop.addEventListener('click', () => clearInterval( interval )); 

function createInterval () {
    return setInterval(()=> {
        timeToNext -= 10;
        if (timeToNext <= 0) {
            timeToNext = timerValue;
            goToNextImg();
        } 
        timer.textContent = (timeToNext / 1000).toFixed(3); 
    })
}

function goToNextImg () {
    images.children[currentImgIndex].hidden = true;
    currentImgIndex++;
    if (currentImgIndex === images.children.length) {
        currentImgIndex = 0;
    }
    images.children[currentImgIndex].hidden = false;
}