
const dataList = [23, 27, 'string', Object, 'banana', 10, 'false'];
//console.log(dataList);


function filterBy(dataList, type) {
    return dataList.filter((item) => {
        if (typeof item !== type) {
            return item;
        }
    });
    
}

const newArray = filterBy(dataList, 'string');
console.log(newArray);

